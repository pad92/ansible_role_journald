Role Name
=========

This role configure journald. Journald is part of systemd and is used to manage
logs. The default behaviour of journald is to log events inside the /run folder.
Doing that doesn't allow journald to store logs once a machine has rebooted.
this role make journald to be persistent at least and can be customised using
variables.


Requirements
------------

having journald on your system :)

Role Variables
--------------
Defaults :

```
journald:
  SystemMaxUse: 1G
  MaxRetentionSec: 604800
```

Maximum size of /var/log/journald will be 1G & logs will be available for 7 days.

Dependencies
------------

none

License
-------

TBD
